﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace testConnectionString.Migrations
{
    public partial class addTestBool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "migrationWorked",
                table: "ToDoItem",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "migrationWorked",
                table: "ToDoItem");
        }
    }
}
