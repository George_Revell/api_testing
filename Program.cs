using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using testConnectionString.Data;
using testConnectionString.Models;


namespace testConnectionString
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;


            try
            {
                var dataSeeder = new DataSeeder();
                dataSeeder.Initialize(services);
            }
            catch (Exception exc)
            {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(exc, "There was an error seeding database");
            }

            host.Run();
            var context = new ToDoListContext(
                          services.GetRequiredService<
                              DbContextOptions<ToDoListContext>>());
            context.ToDoItem.Any();
            context.ToDoItem.Any();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
