﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testConnectionString.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using testConnectionString.Models;

namespace testConnectionString.Data
{
    public class DataSeeder
    {
        public void Initialize(IServiceProvider serviceProvider)
        { 
      
            using (var context = new ToDoListContext(
              serviceProvider.GetRequiredService<
                  DbContextOptions<ToDoListContext>>()))

            {
                
                if (context.ToDoItem.Any() == false)
                {
                    context.AddRange(

                        new ToDoItem()
                        {
                            Name = "testItem1",
                            IsComplete = false
                        },

                        new ToDoItem()
                        {
                            Name = "testItem2",
                            IsComplete = false
                        },

                          new ToDoItem()
                          {
                              Name = "testItem3",
                              IsComplete = false
                          }

                        );
                    context.SaveChanges();
                    //await _context.SaveChangesAsync();



                    if (context.ToDoItem.Any())
                    {
                       
                    }else
                    {

                    }
                }
            }
        }
    }
}
